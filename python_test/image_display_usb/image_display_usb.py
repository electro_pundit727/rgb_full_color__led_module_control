#!/usr/bin/python
import os
import time

from shutil import copyfile, ignore_patterns
from rgbmatrix import RGBMatrix, RGBMatrixOptions
from PIL import Image


def DeleteAllFiles(dest_directory):
    for file_name in os.listdir(dest_directory):
        os.remove(os.path.join(dest_directory, file_name))

if __name__ == '__main__':

    source_directory = '/media/usb/'
    files = os.listdir(source_directory)
    dest_directory = '/home/pi/RHS/python_test/image_display_usb/image/'
    ftype = ('BMP', 'GIF', 'IMG', 'jpeg', 'jpg', 'PNG', 'PPM', 'PSD', 'TIFF')

    DeleteAllFiles(dest_directory)  # Delete all of the previous files in dest directory

    file_list = []

    for file_name in files:
        if file_name.endswith(ftype):
            src = source_directory + file_name
            dst = dest_directory + file_name
            copyfile(src, dst)
            file_list.append(dst)

    file_list.sort()  # Sort copied files in alphabetical order.

    # Configuration for the matrix
    options = RGBMatrixOptions()
    options.rows = 64
    options.chain_length = 2
    options.parallel = 1
    options.hardware_mapping = 'regular'  # If you have an Adafruit HAT: 'adafruit-hat'

    matrix = RGBMatrix(options=options)

    while True:

        for i in range(len(file_list)):
            fname = file_list[i]
            image = Image.open(fname)

            # Make image fit our screen.
            image.thumbnail((matrix.width, matrix.height), Image.ANTIALIAS)
            matrix.SetImage(image.convert('RGB'))
            time.sleep(3)