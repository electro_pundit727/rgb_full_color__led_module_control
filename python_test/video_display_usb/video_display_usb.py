#!/usr/bin/python
import os
from os import listdir
from os.path import isfile, isdir, join
import cv2
import sys
from PIL import Image
import time
import platform
from shutil import copyfile
# import subprocess

if platform.system() != 'Windows':
    from rgbmatrix import RGBMatrix, RGBMatrixOptions


source_directory = '/media/pi/disk/'
dest_directory = '/home/pi/RHS/video_display_usb/video/'
path = './video'

image_types = ['.jpg', '.png', '.bmp', '.jpeg']
video_types = ['.avi', '.mp4', '.3gp', '.mpg']
ftype = ('mp4', 'MP4', 'avi', 'mov', 'wmv', '3GP', '3gp', 'mpg', 'mpeg', 'jpg', 'jpeg')


class LedDisplay:

    matrix = None
    double_buffer = None
    fnames = []
    total = []
    g_length = []
    g_fps = []

    def __init__(self, fnames):

        self.fnames = fnames

        self.config_matrix()

    def config_matrix(self):
        """
            Configuration the LED matrix
        """
        if platform.system() != 'Windows':
            options = RGBMatrixOptions()
            options.rows = 64
            options.chain_length = 8
            options.parallel = 3
            options.hardware_mapping = 'regular'  # If you have an Adafruit HAT: 'adafruit-hat'

            options.brightness = 100
            options.show_refresh_rate = 0
            options.pwm_bits = 11
            options.pwm_lsb_nanoseconds = 130
            options.scan_mode = 0  # This value is 0 by default.

            self.matrix = RGBMatrix(options=options)
            self.double_buffer = self.matrix.CreateFrameCanvas()

    def run(self):
        """

        """
        self.frame_range()
        self.frame_display()

    def frame_range(self):
        """

        """
        self.g_length = [0] * len(self.fnames)
        self.g_fps = [0] * len(self.fnames)
        for i in range(0, len(self.fnames)):
            fn = self.fnames[i]
            base, ext = os.path.splitext(fn)

            if ext.lower() in video_types:
                g_cap = cv2.VideoCapture(fn)
                if not g_cap.isOpened():
                    sys.stderr.write("Failed to Open Capture device. Quitting.\n")
                else:
                    self.g_length[i] = int(g_cap.get(cv2.CAP_PROP_FRAME_COUNT))
                    self.g_fps[i] = int(g_cap.get(cv2.CAP_PROP_FPS))
                    # self.g_length.append(int(g_cap.get(cv2.CAP_PROP_FRAME_COUNT)))
                    # self.g_fps.append(int(g_cap.get(cv2.CAP_PROP_FPS)))
                    print i
                    print "    file:", fn, "length:", self.g_length[i], "fps:", self.g_fps[i]

                    imgs = []
                    pos = 0
                    while pos < self.g_length[i] - 3:
                        print pos, self.g_length[i]
                        # start = datetime.now()

                        _, frame = g_cap.read()
                        frame = cv2.resize(frame, (self.matrix.width, self.matrix.height))
                        image = Image.fromarray(frame)
                        r, g, b = image.split()
                        image = Image.merge('RGB', (b, r, g))
                        imgs.append(image)
                        pos += 1

                        # end = datetime.now()
                        # interval = (end - start).microseconds
                        # print int(1000000 / interval)
                    self.total.append(imgs)

            if ext.lower() in image_types:
                imgs = ''
                self.total.append(imgs)
                # self.g_length.append(0)
                # self.g_fps.append(0)
                print 'img_file'
                print i

    def frame_display(self):
        """

        """
        while True:
            i = 0
            print 'Now is playing...'
            for i in range(0, len(self.total)):
                if self.total[i] == '':
                    self.image_display(self.fnames[i])
                else:
                    self.video_display(self.total[i], i)

    def image_display(self, img_file):
        """

        """
        print 'img_file'
        frame = cv2.imread(img_file)
        frame = cv2.resize(frame, (self.matrix.width, self.matrix.height))  # resize the resolution of the frame
        image = Image.fromarray(frame)  # convert type of image from cv2 to PIL image
        r, g, b = image.split()
        image = Image.merge('RGB', (b, r, g))

        self.double_buffer.SetImage(image)
        self.double_buffer = self.matrix.SwapOnVSync(self.double_buffer)
        time.sleep(5)

    def video_display(self, vid_file, i):
        """

        """
        print 'video_file'
        pos = 0
        while pos < (self.g_length[i] - 4):
            self.double_buffer.SetImage(vid_file[pos])
            self.double_buffer = self.matrix.SwapOnVSync(self.double_buffer)
            pos += 1

def DeleteAllFiles(dest_directory):
    for file_name in listdir(dest_directory):
        os.remove(os.path.join(dest_directory, file_name))
    print "Previous files in video directory are deleted"


if __name__ == '__main__':

    # rv1 = subprocess.call("grep -qs '/mnt/usbdrive' /proc/mounts", shell=True)
    # rv2 = subprocess.call("mount | grep /mnt/usbdrive", shell=True)
    # rv3 = subprocess.call("mountpoint -q /mnt/usbdrive", shell=True)    #
    # print rv1, rv2, rv3
    # if rv3 == 0:

    if isdir("/media/pi/disk"):
        print "Usb is connected."
        DeleteAllFiles(dest_directory)  # Delete all of the previous files in dest directory
        files = listdir(source_directory)

        file_list = []
        for file_name in files:
            if file_name.endswith(ftype):
                src = source_directory + file_name
                dst = dest_directory + file_name
                print 'file copying : ', file_name
                copyfile(src, dst)
                file_list.append(dst)
        file_list.sort()  # Sort copied files in alphabetical order.
        print "Copy has done."
    else:
        print "Usb is not connected."

    files = [join(path, f) for f in listdir(path) if isfile(join(path, f))]
    files.sort()
    if len(files) == 0:
        print "No supported file in current directory."
    else:
        ledctrl = LedDisplay(files)
        ledctrl.run()