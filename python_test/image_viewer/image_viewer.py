#!/usr/bin/env python
import time
import sys

from rgbmatrix import RGBMatrix, RGBMatrixOptions
from PIL import Image

if __name__ == '__main__':

    image = Image.open('d.jpg')

    # Configuration for the matrix
    options = RGBMatrixOptions()
    options.rows = 64
    options.chain_length = 8
    options.parallel = 3
    options.hardware_mapping = 'regular'  # If you have an Adafruit HAT: 'adafruit-hat'

    # options.brightness = 100  # This value is 100 by default.
    # options.show_refresh_rate = 100  #
    # options.pwm_bits = 11  # This value is 11 by default.
    # options.pwm_lsb_nanoseconds = 100  # This value is 130 by default.
    #                                    # Good values for full-color display(PWM=11) are
    #                                    # somewhere between 100 and 300.
    # options.scan_mode = 0  # This value is 0 by default.

    matrix = RGBMatrix(options=options)

    # RGB -> RBG
    r, g, b = image.split()
    image = Image.merge("RGB", (r, b, g))  # convert RGB to RBG


    # Make image fit our screen.
    # image.thumbnail((matrix.width, matrix.height), Image.ANTIALIAS)

    # resize the size of image to the size of matrix
    image = image.resize((matrix.width, matrix.height), Image.ANTIALIAS)
    matrix.SetImage(image.convert('RGB'))

    try:
        print("Press CTRL-C to stop.")
        while True:
            time.sleep(100)
    except KeyboardInterrupt:
        sys.exit(0)
