#!/usr/bin/python
import os
from os import listdir
from os.path import isfile, join
import cv2
import sys
from rgbmatrix import RGBMatrix, RGBMatrixOptions
from PIL import Image
import time
from datetime import datetime

img_types = ['.jpg', '.png', '.bmp']
video_types = ['.avi', '.mp4', '.3gp', '.mpg', '.MP4', '.3GP']

def display(files):

    # Configuration for the matrix
    print "config the matrix"
    options = RGBMatrixOptions()
    options.rows = 64
    options.chain_length = 8
    options.parallel = 3
    options.hardware_mapping = 'regular'  # If you have an Adafruit HAT: 'adafruit-hat'

    options.brightness = 100
    options.show_refresh_rate = 0
    options.pwm_bits = 11
    options.pwm_lsb_nanoseconds = 130
    options.scan_mode = 0  # This value is 0 by default.

    matrix = RGBMatrix(options=options)
    double_buffer = matrix.CreateFrameCanvas()

    i = 0
    while True:
        i = (i + 1) % len(files)

        file = files[i]
        print file
        fn, ext = os.path.splitext(file)
        # print "file name", file
        if ext.lower() in img_types:
            """ image display """
            frame = cv2.imread(file)
            frame = cv2.resize(frame, (matrix.width, matrix.height))  # resize the resolution of the frame

            image = Image.fromarray(frame)  # convert type of image from cv2 to PIL image
            r, g, b = image.split()
            image = Image.merge('RGB', (b, r, g))

            double_buffer.SetImage(image)
            double_buffer = matrix.SwapOnVSync(double_buffer)
            time.sleep(5)

        if ext.lower() in video_types:
            """ video display """

            g_cap = cv2.VideoCapture(file)
            if not g_cap.isOpened():
                sys.stderr.write("Faled to Open Capture device. Quitting.\n")
                continue

            # g_length = int(g_cap.get(cv2.CAP_PROP_FRAME_COUNT))
            g_length = min(500, int(g_cap.get(cv2.CAP_PROP_FRAME_COUNT)))
            print "    file:", file, "length:", g_length

            imgs = []
            pos = 0
            while pos < g_length - 3:
                print pos, g_length
                start = datetime.now()

                _, frame = g_cap.read()
                frame = cv2.resize(frame, (matrix.width, matrix.height))
                image = Image.fromarray(frame)
                r, g, b = image.split()
                image = Image.merge('RGB', (b, r, g))
                imgs.append(image)
                pos += 1

                end = datetime.now()
                interval = (end - start).microseconds
                print int(1000000 / interval)

            i = 0
            while True:
                start = datetime.now()
                i = (i + 1) % (g_length - 4)
                double_buffer.SetImage(imgs[i])
                double_buffer = matrix.SwapOnVSync(double_buffer)
                end = datetime.now()
                interval = (end - start).microseconds
                print int(1000000 / interval)


if __name__ == '__main__':

    path = './videos'

    files = [join(path, f) for f in listdir(path) if isfile(join(path, f))]
    if len(files) == 0:
        print "No supported file in current directory.", path
    else:
        display(files)

