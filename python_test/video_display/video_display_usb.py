#!/usr/bin/python
import os
import time
import cv2

from shutil import copyfile, ignore_patterns
from rgbmatrix import RGBMatrix, RGBMatrixOptions
from PIL import Image

if __name__ == '__main__':

    source_directory = './videos'
    files = os.listdir(source_directory)    
    
    ftype = ('mp4', 'MP4', 'avi', 'mov', 'wmv', '3GP', 'mpg', 'mpeg')
    file_list = []
    
    for file_name in files:
        if file_name.endswith(ftype):
            src = source_directory + "/" + file_name
            file_list.append(src)

    file_list.sort()  # Sort copied files in alphabetical order.

    # Configuration for the matrix
    options = RGBMatrixOptions()
    options.rows = 64
    options.chain_length = 8
    options.parallel = 3
    options.hardware_mapping = 'regular'  # If you have an Adafruit HAT: 'adafruit-hat'

    matrix = RGBMatrix(options=options)

    while True:

        for i in range(len(file_list)):
            fname = file_list[i]
            print fname

            # create object of video capture
            cap = cv2.VideoCapture(fname)
            length = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))  # get total number of frame

            print length
            frameid = 0
            while True:

                ret, frame = cap.read()  # Capture frame-by-frame
                frameid += 1

                if frameid == length - 2:
                    break

                cv2_frame = cv2.resize(frame, (matrix.width, matrix.height))  # change the solution of the frame
                cv2_frame = cv2.cvtColor(cv2_frame, cv2.COLOR_BGR2RGB)  # convert color method from BGR to RGB

                # RGB -> RBG
                new_frame = cv2_frame.copy()
                new_frame[:, :, 0] = cv2_frame[:, :, 0]
                new_frame[:, :, 2] = cv2_frame[:, :, 1]
                new_frame[:, :, 1] = cv2_frame[:, :, 2]

                image = Image.fromarray(new_frame)  # convert type of image from cv2 to PIL image

                # Make image fit our screen.
                image.thumbnail((matrix.width, matrix.height), Image.ANTIALIAS)
                matrix.SetImage(image.convert('RGB'))

                # cv2.waitKey(int((1000/fps)/3))  # set time of display frame
                cv2.waitKey(20)  # set time of display frame

            # When everything done, release the capture
            cap.release()
