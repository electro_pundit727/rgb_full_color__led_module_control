#!/usr/bin/python
import os
from os import listdir
from os.path import isfile, join
import cv2
import sys
from rgbmatrix import RGBMatrix, RGBMatrixOptions
from PIL import Image
import time
import threading
from datetime import datetime
import Queue

image_types = ['.jpg', '.png', '.bmp']
video_types = ['.avi', '.mp4', '.3gp', '.mpg']
path = './videos'


class LedDisplay:

    matrix = None
    double_buffer = None
    fnames = []
    img_queue = None
    state = ''
    thread = None

    def __init__(self, fnames):

        self.fnames = fnames

        self.img_queue = Queue.Queue()
        self.config_matrix()

    def config_matrix(self):
        """
            Configuration the LED matrix
        """
        options = RGBMatrixOptions()
        options.rows = 64
        options.chain_length = 8
        options.parallel = 3
        options.hardware_mapping = 'regular'  # If you have an Adafruit HAT: 'adafruit-hat'

        options.brightness = 100
        options.show_refresh_rate = 0
        options.pwm_bits = 11
        options.pwm_lsb_nanoseconds = 130
        options.scan_mode = 0  # This value is 0 by default.

        self.matrix = RGBMatrix(options=options)
        self.double_buffer = self.matrix.CreateFrameCanvas()

    def run(self):
        """
        Range frame of video, run display_thread thread.
        """
        self.state = True
        self.thread = threading.Thread(target=self.frame_display)
        self.thread.start()
        self.frame_range()

    def frame_range(self):
        """
        Put each frame of each file to queue.
        """
        i = 0
        while True:

            i = (i + 1) % len(self.fnames)
            fn = self.fnames[i]
            base, ext = os.path.splitext(fn)

            if ext.lower() in image_types:
                self.image_frame_range(fn)
            if ext.lower() in video_types:
                self.video_frame_range(fn)

    def image_frame_range(self, img_file):

        frame = cv2.imread(img_file)
        frame = cv2.resize(frame, (self.matrix.width, self.matrix.height))  # resize the resolution of the frame
        image = Image.fromarray(frame)  # convert type of image from cv2 to PIL image
        r, g, b = image.split()
        image = Image.merge('RGB', (b, r, g))

        self.state = False

        # self.img_queue.put(image)
        self.double_buffer.SetImage(image)
        self.double_buffer = self.matrix.SwapOnVSync(self.double_buffer)
        time.sleep(5)

        self.state = True

    def video_frame_range(self, vid_file):

        g_cap = cv2.VideoCapture(vid_file)

        if not g_cap.isOpened():
            sys.stderr.write("Failed to Open Capture device. Quitting.\n")
        else:
            g_length = int(g_cap.get(cv2.CAP_PROP_FRAME_COUNT))  # g_length = int(g_cap.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT))
            g_fps = int(g_cap.get(cv2.CAP_PROP_FPS))  # g_fps = int(g_cap.get(cv2.cv.CV_CAP_PROP_FPS))
            print "    file:", vid_file, "length:", g_length, "fps:", g_fps

            pos = 0
            while pos < 500:

                start = datetime.now()

                _, frame = g_cap.read()
                frame = cv2.resize(frame, (self.matrix.width, self.matrix.height))
                image = Image.fromarray(frame)
                r, g, b = image.split()
                image = Image.merge('RGB', (b, r, g))

                end = datetime.now()
                # interval = end - start
                # t_interval = interval.total_seconds()

                interval = (end - start).microseconds

                # print pos, g_length, int(1000000 / interval)

                # delay = 1.0/g_fps - t_interval
                # print t_interval, delay
                # time.sleep(delay)
                print "put"
                self.img_queue.put(image)

                # print self.img_queue.qsize()
                pos += 1

    def frame_display(self):
        """
        Get each frame from the queue and display on the led matrix.
        """
        while self.state:
            try:
                start = datetime.now()
                print "pop"
                frame = self.img_queue.get()

                self.double_buffer.SetImage(frame)
                self.double_buffer = self.matrix.SwapOnVSync(self.double_buffer)

                end = datetime.now()
                interval = (end - start).microseconds
                # sys.stdout.write("\r" + "        " + str(1000000 / interval))
                # sys.stdout.flush()

            except Queue.Empty:
                print "expect", self.img_queue.qsize()
                pass

    # def kill_thread(self):


if __name__ == '__main__':

    files = [join(path, f) for f in listdir(path) if isfile(join(path, f))]
    if len(files) == 0:
        print "No supported file in current directory."
    else:
        ledctrl = LedDisplay(files)
        ledctrl.run()
